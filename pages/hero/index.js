import illust1 from '../../public/illust1.jpg'
import Image from 'next/image'
import { ReactElement } from 'react'
import { faCoffee, faPoo, faFish } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default function Hero() {
    return (
        <div className='w-full py-20'>
            <div className='container mx-auto lg:flex'>
                <div className='lg:w-2/4 lg:pl-40 px-16'>
                    <h1 className='pt-14 text-3xl font-extrabold font-poppins'>Lorem ipsum dolor sit amet consectetur</h1>
                    <p className='pt-3 text-slate-500 text-sm font-poppins'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in ultricies felis, iaculis elementum libero. Nulla tempus viverra odio, ut porta eros imperdiet eget. In in massa feugiat est feugiat maximus ac vel est. Mauris porttitor lorem sit amet tortor blandit accumsan. Sed nulla mi, tempus nec suscipit ac, pulvinar nec justo. Cras tellus sem, ultrices porta libero facilisis, ultrices congue ligula. Aliquam venenatis, sem eu molestie imperdiet, lacus ipsum iaculis risus, quis vehicula nulla sapien eget libero. Phasellus maximus eget erat ac pretium. Donec suscipit lectus molestie turpis posuere luctus. Suspendisse dapibus diam et quam vestibulum, nec laoreet nulla egestas</p>
                    <div className='pt-5 flex items-center'>
                    <button className='border py-2 px-5 bg-primary rounded font-poppins'>Hubungi Saya</button>
                    <FontAwesomeIcon icon={faPoo} className='pl-5 text-3xl'/>
                    <FontAwesomeIcon icon={faFish} className='pl-5 text-3xl'/>
                    </div>
                </div>
                <div className='w-2/4 flex justify-center py-[100px] z-0 hidden xl:flex'>
                    <Image src={illust1} width={350} height={350}/>
                </div>
            </div>
        </div>
    )
}