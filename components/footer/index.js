import { faFacebook, faWhatsapp, faDiscord } from "@fortawesome/free-brands-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
export default function Footer() {
    return (
        <div className="bg-black py-20">
            <div className="flex justify-center">
                <div className="flex items-center">
                    <FontAwesomeIcon icon={faFacebook} className="text-white text-3xl mx-3"/>
                    <FontAwesomeIcon icon={faWhatsapp} className='text-white text-3xl mx-3'/>
                    <FontAwesomeIcon icon={faDiscord} className='text-white text-3xl mx-3'/>
                </div>
            </div>
                <div className="text-primary text-sm flex justify-center pt-4 font-poppins">
                    <p className="mx-2">info</p>
                    <p className="mx-2">support</p>
                </div>
                <div className="text-primary text-sm flex justify-center pt-2 font-poppins">
                    <p className="mx-2">Term of Use</p>
                    <p className="mx-2">Privacy Policy</p>
                </div>
                <div className="text-cyan-700 text-xs flex justify-center pt-2 font-poppins">
                    <p className="mx-2">&copy; 2022 Cartix Official</p>
                </div>
        </div>
    )
}