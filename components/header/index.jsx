export default function Header() {
    return (
        <div className="w-full bg-black py-5 px-4 xl:px-0 fixed z-50 font-poppins">
            <div className="container mx-auto flex justify-between items-center ">
                <div className="text-white w-1/4">
                    <h1 className="text-2xl font-extrabold text-primary">Hapis</h1>
                </div>
                <div className="text-white justify-center w-2/4 hidden xl:flex">
                    <p className="px-2 text-sm uppercase font-semibold">about</p>
                    <p className="px-2 text-sm uppercase font-semibold">services</p>
                    <p className="px-2 text-sm uppercase font-semibold">contact us</p>
                </div>
                <div className="w-1/4 text-right">
                    <span className="text-black text-sm bg-primary py-2 px-5 rounded-sm font-bold cursor-pointer">LOGIN</span>
                </div>
            </div>
        </div>
    )
}